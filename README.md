# Test technique 


### Description
  - Mise en place skeleton Symfony 4 
  - Mise en place API platform
  - Modèle (incomplet) représentant les sociétés 
  - Commande d'import des fichiers
  - Filtre Api
  
### Installation

- 1 : Cloner le projet GIT
- 2 : Setup BDD
- 3 : Execution import
- 4 : Accès à l'API

Setup BDD dans le .env
```sh
DATABASE_URL=mysql://db_user:db_password@127.0.0.1:3306/db_name
```
Initilisation de la BDD
```sh
php bin/console doctrine:database:create
php bin/console doctrine:migration:migrate
php bin/console server:run
```
Import des données
```sh
php bin/console app:import
```
Accès à l'API
```sh
http://127.0.0.1:8000/api/societes.json?nomRaisonSoc=CLAUDE
http://127.0.0.1:8000/api/societes.json?siegeAdrNomVoie=paris
http://127.0.0.1:8000/api/societes.json?siegeAdrNomVoie=paris&nomRaisonSoc=ISLA```
```

### Améliorations possibles
- Vérification des fichiers
- Ajout ou non des sociétés incomplètes
- Dédoublonnage
- Recherche Elasticsearch
- ...
