<?php

namespace App\Command;

use App\Entity\Rep;
use App\Entity\Societe;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Config\Util\XmlUtils;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\Finder;

class ImportCommand extends Command
{
    protected static $defaultName = 'app:import';

    private $path;
    private $em;

    public function __construct(string $path, EntityManagerInterface $entityManager)
    {
        parent::__construct();
        $this->path = $path;
        $this->em   = $entityManager;
    }

    protected function configure()
    {
        $this
            ->setDescription('Import IMR files');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $finder = new Finder();
        $finder->files()->in($this->path);
        foreach ($finder as $file) {
            $content = $this->convertXmlToArray($file->getRealPath());
            $this->importContent($content, $output);
        }
    }

    /**
     * @param $path
     *
     * @return array
     */
    protected function convertXmlToArray(string $path): array
    {
        return XmlUtils::convertDomElementToArray(XmlUtils::loadFile($path)->firstChild);
    }

    /**
     * @param array           $content
     * @param OutputInterface $output
     *
     * @throws \Exception
     */
    protected function importContent(array $content, OutputInterface $output)
    {
        $batchCount = 0;
        $batchLimit = 100;
        foreach ($content['grf']['societe'] as $societeContent) {
            $societe = new Societe();
            $societe->setSiren($societeContent['siren'] ?? null);
            $societe->setNomRaisonSoc($societeContent['idt']['nom_raison_soc'] ?? null);
            $societe->setApeNaf($societeContent['idt']['ape_naf'] ?? null);
            $societe->setCapDevise($societeContent['idt']['cap']['devise'] ?? null);
            $societe->setCapMontant($societeContent['idt']['cap']['montant'] ?? null);
            $societe->setCodeFormJur($societeContent['idt']['code_form_jur'] ?? null);
            if (isset($societeContent['dat_donnees'])) {
                $societe->setDatDonnees(new \DateTime($societeContent['dat_donnees']));
            }
            $societe->setDiversStatusEditExtrait($societeContent['idt']['divers']['statut_edit_extrait'] ?? null);
            if (isset($societeContent['idt']['immat']['dat'])) {
                $societe->setImmatDate(new \DateTime($societeContent['idt']['immat']['dat']));
            }
            $societe->setNumGestion($societeContent['num_gestion'] ?? null);
            if (isset($societeContent['idt']['siege']['adr_1'])){
                $societe->setSiegeAdrNomVoie(implode(' ', $societeContent['idt']['siege']['adr_1']));
            }

            if (isset($societeContent['reps'])) {
                $repContent = $societeContent['reps']['rep'];
                    $rep = new Rep();
                    $rep->setNomDenom($repContent['nom_denom'] ?? null);
                    if (isset($repContent['pers_physique']['dat_naiss'])) {
                        $rep->setPersPhysiqueDateNaiss(new \DateTime($repContent['pers_physique']['dat_naiss']));
                    }
                    $rep->setPersPhysiqueLieuNaiss($repContent['pers_physique']['lieu_naiss'] ?? null);
                    $rep->setPrenom($repContent['prenom'] ?? null);
                    $rep->setQual($repContent['qual'] ?? null);
                    $rep->setRaisNom($repContent['rais_nom'] ?? null);
                    $rep->setTypeDir($repContent['type_dir'] ?? null);
                    $rep->setSociete($societe);
                    $this->em->persist($rep);
            }

            $this->em->persist($societe);
            $batchCount++;
            $output->writeln('<fg=green>Société importée</>');
            if (($batchCount % $batchLimit) == 0) {
                $this->em->flush();
                $this->em->clear();
            }
        }
    }
}
