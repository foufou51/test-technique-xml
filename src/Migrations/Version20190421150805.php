<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Class Version20190421150805
 */
final class Version20190421150805 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Setup BDD';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE societe (id INT AUTO_INCREMENT NOT NULL, rcs VARCHAR(100) DEFAULT NULL, nom_raison_soc VARCHAR(255) DEFAULT NULL, code_form_jur INT DEFAULT NULL, immat_date DATE DEFAULT NULL, ape_naf VARCHAR(20) DEFAULT NULL, cap_montant NUMERIC(10, 0) DEFAULT NULL, cap_devise VARCHAR(20) DEFAULT NULL, divers_status_edit_extrait VARCHAR(20) DEFAULT NULL, dat_donnees DATE DEFAULT NULL, num_gestion VARCHAR(50) DEFAULT NULL, siren VARCHAR(20) DEFAULT NULL, siege_adr_nom_voie VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE rep (id INT AUTO_INCREMENT NOT NULL, societe_id INT DEFAULT NULL, qual VARCHAR(10) DEFAULT NULL, rais_nom VARCHAR(255) DEFAULT NULL, nom_denom VARCHAR(255) DEFAULT NULL, prenom VARCHAR(255) DEFAULT NULL, type_dir VARCHAR(10) DEFAULT NULL, pers_physique_date_naiss DATE DEFAULT NULL, pers_physique_lieu_naiss VARCHAR(255) DEFAULT NULL, pers_physique_rep_nom_usage VARCHAR(255) DEFAULT NULL, INDEX IDX_E0BB8BF2FCF77503 (societe_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE rep ADD CONSTRAINT FK_E0BB8BF2FCF77503 FOREIGN KEY (societe_id) REFERENCES societe (id)');
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE rep DROP FOREIGN KEY FK_E0BB8BF2FCF77503');
        $this->addSql('DROP TABLE societe');
        $this->addSql('DROP TABLE rep');
    }
}
