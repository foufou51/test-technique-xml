<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SocieteRepository")
 * @ApiResource(
 *     collectionOperations={"get"},
 *     itemOperations={"get"}
 * )
 * @ApiFilter(SearchFilter::class, properties={"id": "exact", "siegeAdrNomVoie": "partial", "nomRaisonSoc": "partial"})
 */
class Societe
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $rcs;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nomRaisonSoc;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $code_form_jur;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $immatDate;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $apeNaf;

    /**
     * @ORM\Column(type="decimal", nullable=true)
     */
    private $capMontant;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $capDevise;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $diversStatusEditExtrait;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Rep", mappedBy="societe")
     */
    private $reps;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $datDonnees;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $numGestion;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $siren;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $siegeAdrNomVoie;

    public function __construct()
    {
        $this->reps = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRcs(): ?string
    {
        return $this->rcs;
    }

    public function setRcs(?string $rcs): self
    {
        $this->rcs = $rcs;

        return $this;
    }

    public function getNomRaisonSoc(): ?string
    {
        return $this->nomRaisonSoc;
    }

    public function setNomRaisonSoc(?string $nomRaisonSoc): self
    {
        $this->nomRaisonSoc = $nomRaisonSoc;

        return $this;
    }

    public function getCodeFormJur(): ?int
    {
        return $this->code_form_jur;
    }

    public function setCodeFormJur(?int $code_form_jur): self
    {
        $this->code_form_jur = $code_form_jur;

        return $this;
    }

    public function getImmatDate(): ?\DateTimeInterface
    {
        return $this->immatDate;
    }

    public function setImmatDate(?\DateTimeInterface $immatDate): self
    {
        $this->immatDate = $immatDate;

        return $this;
    }

    public function getApeNaf(): ?string
    {
        return $this->apeNaf;
    }

    public function setApeNaf(?string $apeNaf): self
    {
        $this->apeNaf = $apeNaf;

        return $this;
    }

    public function getCapMontant()
    {
        return $this->capMontant;
    }

    public function setCapMontant(?float $capMontant): self
    {
        $this->capMontant = $capMontant;

        return $this;
    }

    public function getCapDevise(): ?string
    {
        return $this->capDevise;
    }

    public function setCapDevise(?string $capDevise): self
    {
        $this->capDevise = $capDevise;

        return $this;
    }

    public function getDiversStatusEditExtrait(): ?string
    {
        return $this->diversStatusEditExtrait;
    }

    public function setDiversStatusEditExtrait(?string $diversStatusEditExtrait): self
    {
        $this->diversStatusEditExtrait = $diversStatusEditExtrait;

        return $this;
    }

    /**
     * @return Collection|Rep[]
     */
    public function getReps(): Collection
    {
        return $this->reps;
    }

    public function addRep(Rep $rep): self
    {
        if (!$this->reps->contains($rep)) {
            $this->reps[] = $rep;
            $rep->setSociete($this);
        }

        return $this;
    }

    public function removeRep(Rep $rep): self
    {
        if ($this->reps->contains($rep)) {
            $this->reps->removeElement($rep);
            // set the owning side to null (unless already changed)
            if ($rep->getSociete() === $this) {
                $rep->setSociete(null);
            }
        }

        return $this;
    }

    public function getDatDonnees(): ?\DateTimeInterface
    {
        return $this->datDonnees;
    }

    public function setDatDonnees(?\DateTimeInterface $datDonnees): self
    {
        $this->datDonnees = $datDonnees;

        return $this;
    }

    public function getNumGestion(): ?string
    {
        return $this->numGestion;
    }

    public function setNumGestion(?string $numGestion): self
    {
        $this->numGestion = $numGestion;

        return $this;
    }

    public function getSiren(): ?string
    {
        return $this->siren;
    }

    public function setSiren(?string $siren): self
    {
        $this->siren = $siren;

        return $this;
    }

    public function getSiegeAdrNomVoie(): ?string
    {
        return $this->siegeAdrNomVoie;
    }

    public function setSiegeAdrNomVoie(?string $siegeAdrNomVoie): self
    {
        $this->siegeAdrNomVoie = $siegeAdrNomVoie;

        return $this;
    }
}
