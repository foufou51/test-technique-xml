<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ApiResource(
 *     collectionOperations={"get"},
 *     itemOperations={"get"}
 * )
 * @ORM\Entity(repositoryClass="App\Repository\RepRepository")
 */
class Rep
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $qual;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $raisNom;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nomDenom;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $prenom;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $typeDir;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $persPhysiqueDateNaiss;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $persPhysiqueLieuNaiss;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $persPhysiqueRepNomUsage;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Societe", inversedBy="reps")
     */
    private $societe;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQual(): ?string
    {
        return $this->qual;
    }

    public function setQual(?string $qual): self
    {
        $this->qual = $qual;

        return $this;
    }

    public function getRaisNom(): ?string
    {
        return $this->raisNom;
    }

    public function setRaisNom(?string $raisNom): self
    {
        $this->raisNom = $raisNom;

        return $this;
    }

    public function getNomDenom(): ?string
    {
        return $this->nomDenom;
    }

    public function setNomDenom(?string $nomDenom): self
    {
        $this->nomDenom = $nomDenom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(?string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getTypeDir(): ?string
    {
        return $this->typeDir;
    }

    public function setTypeDir(?string $typeDir): self
    {
        $this->typeDir = $typeDir;

        return $this;
    }

    public function getPersPhysiqueDateNaiss(): ?\DateTimeInterface
    {
        return $this->persPhysiqueDateNaiss;
    }

    public function setPersPhysiqueDateNaiss(?\DateTimeInterface $persPhysiqueDateNaiss): self
    {
        $this->persPhysiqueDateNaiss = $persPhysiqueDateNaiss;

        return $this;
    }

    public function getPersPhysiqueLieuNaiss(): ?string
    {
        return $this->persPhysiqueLieuNaiss;
    }

    public function setPersPhysiqueLieuNaiss(?string $persPhysiqueLieuNaiss): self
    {
        $this->persPhysiqueLieuNaiss = $persPhysiqueLieuNaiss;

        return $this;
    }

    public function getPersPhysiqueRepNomUsage(): ?string
    {
        return $this->persPhysiqueRepNomUsage;
    }

    public function setPersPhysiqueRepNomUsage(?string $persPhysiqueRepNomUsage): self
    {
        $this->persPhysiqueRepNomUsage = $persPhysiqueRepNomUsage;

        return $this;
    }

    public function getSociete(): ?Societe
    {
        return $this->societe;
    }

    public function setSociete(?Societe $societe): self
    {
        $this->societe = $societe;

        return $this;
    }
}
